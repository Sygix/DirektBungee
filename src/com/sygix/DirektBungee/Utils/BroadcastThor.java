/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Utils;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class BroadcastThor {
	
	public static String prefix(){
		return ChatColor.GOLD+"[Thor] ";
	}
	
	@SuppressWarnings("deprecation")
	public static void send(String s){
		for(ProxiedPlayer pls : ProxyServer.getInstance().getPlayers()){
    		if((pls.hasPermission("DirektServ.helper")) || (pls.hasPermission("DirektServ.architecte")) || (pls.hasPermission("DirektServ.developpeur"))){
    			pls.sendMessage(prefix() + s);
    		}
    	}
	}

}
