/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Utils;

public enum ServerState {
	
	OPEN(true), MAINTENANCE(false), BETA(false);
	
	private static ServerState currentState;
	private boolean canJoin;
	
	ServerState(boolean s) {
		canJoin = s;
	}
	
	public static void setState(ServerState s) {
		currentState = s;
	}
	
	public static boolean isState(ServerState s) {
		return currentState == s;
	}
	
	public static ServerState getState() {
		return currentState;
	}
	
	public boolean canJoin() {
		return canJoin;
	}
}
