/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Utils;

import net.md_5.bungee.api.ChatColor;

public class GetArgsMessage {
	
	public static String getMessage(String[] args, int x)
    {
        StringBuilder sb = new StringBuilder();
        for(int i = x; i < args.length; i++)
        {
            sb.append(args[i]).append( i >= args.length - 1 ? "" : " " );
        }
        return sb.toString();
    }
	
	public static String fixColors(String s){
		s = ChatColor.translateAlternateColorCodes('&', s);
		return s;
	}

}
