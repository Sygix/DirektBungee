/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Utils;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class TabList {
	
	public static void updatetablist(int count){
		for(ProxiedPlayer p : ProxyServer.getInstance().getPlayers()){
			p.setTabHeader(
	                new ComponentBuilder(ChatColor.translateAlternateColorCodes(
	                        '&', "\n          &3Vous �tes sur &8&lDirekt&4&lServ &7| &6&lplay.direktserv.fr          \n")).create(),
	                new ComponentBuilder(ChatColor.translateAlternateColorCodes(
	                		'&', "\n          &awww.direktserv.fr &7| &9ts.direktserv.fr          \n          &cJoueur(s) : &5"+count+"/101          ")).create());
		}
	}

}
