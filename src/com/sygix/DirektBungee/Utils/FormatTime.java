/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Utils;

public class FormatTime {
	
	public static String formatminutes(long t){
		return t/30/24/60+ " mois "+ t/24/60%30 + " jour(s) " + t/60%24 + " heure(s) " + t%60 + " minute(s)";
	}

}
