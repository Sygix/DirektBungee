/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.PluginManager;

import com.sygix.DirektBungee.DirektBungee;

public class Reload extends Command {
	
	public Reload(String name) {
        super(name);
    }

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		if(sender.hasPermission("DirektServ.admin")){
			PluginManager pm = new PluginManager(ProxyServer.getInstance());
			DirektBungee.getInstance().onDisable();
			DirektBungee.getInstance().onEnable();
			pm.enablePlugins();
			sender.sendMessage("�cVous venez de reload DirektBungee.");
		}

	}

}