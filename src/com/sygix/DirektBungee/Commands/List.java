/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Command;

public class List extends Command {
	
	public List(String name) {
        super(name);
    }

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		int n = ProxyServer.getInstance().getOnlineCount();
		sender.sendMessage(ChatColor.AQUA+"Il y a actuellement "+n+" joueur(s) connect� !");
	}

}
