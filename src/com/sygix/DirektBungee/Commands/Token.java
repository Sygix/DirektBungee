/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Commands;

import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.config.Configuration;

import com.sygix.DirektBungee.DirektBungee;
import com.sygix.DirektBungee.SQL.SQLGestion;
import com.sygix.DirektBungee.Utils.GetArgsMessage;


public class Token extends Command {

	public Token(String name) {
		super(name);
	}
	
	private Configuration getTokenConfig(){
		return DirektBungee.token;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		
		cal.setTime(date);
		if(args[0].equalsIgnoreCase("create")){
			if(args.length == 3){
				String[] code = args[1].split("-");
				if(code.length == 4){
					if(getTokenConfig().get("Tokens."+args[1]) != null){
						sender.sendMessage("§cCe token existe déjà.");
						return;
					}else{
						if(code[0].equalsIgnoreCase("C") || code[0].equalsIgnoreCase("G") || code[0].equalsIgnoreCase("H")){
							DirektBungee.token.set("Tokens."+args[1]+".Expiry", args[2]);
							DirektBungee.SaveConfig();
							sender.sendMessage("§aToken créé.");
							return;
						}else{
							sender.sendMessage("§cToken Invalide.");
							return;
						}
					}
				}else{
					sender.sendMessage("§cToken Invalide, il ne contient pas assez d'argument.");
					return;
				}
			}else{
				sender.sendMessage("§cPas assez d'arguments. /token create <token> <01/01/1970>");
				return;
			}
		}else if(args[0].equalsIgnoreCase("auto")){
			//Génération random d'un token
		}else {
			if(!(sender instanceof ProxiedPlayer))return;
			ProxiedPlayer p = (ProxiedPlayer) sender;

			args[0].replaceAll(",", "."); //replace les , pour les doubles

			String[] code = args[0].split("-");

			if(code.length == 4){
				getTokenConfig().getSection("Tokens").getKeys().forEach(code1 -> {
					if(GetArgsMessage.getMessage(code, 0).equalsIgnoreCase(code1)){
						if(getTokenConfig().getString("Tokens."+code1+".Expiry") != null){
							Date expiry;
							try {
								expiry = sdf.parse(getTokenConfig().getString("Tokens."+code1+".Expiry"));
								if(cal.before(expiry)){
									cal.add(Calendar.DAY_OF_WEEK, Integer.parseInt(code[3]));
									switch(code[0]){
										case "C":
											getTokenConfig().set("Tokens."+code1, "");
											DirektBungee.SaveConfig();
											SQLGestion.setCoins(p, SQLGestion.getCoins(p) + Double.parseDouble(code[2]));
											p.sendMessage("§a"+code[2]+" ont été ajouté à votre compte !");
											break;
										case "G":
											getTokenConfig().set("Tokens."+code1, "");
											DirektBungee.SaveConfig();
											SQLGestion.setDateBuyGrade(p, df.format(cal.getTime()));
											SQLGestion.setGrade(p, code[2]);
											DirektBungee.getInstance().getProxy().getPluginManager().dispatchCommand(DirektBungee.getInstance().getProxy().getConsole(), "bp user "+p.getName()+" addgroup "+code[2]);
											p.sendMessage("§a Votre nouveau grade est "+code[2]+" !");
											break;
										case "H":
											getTokenConfig().set("Tokens."+code1, "");
											DirektBungee.SaveConfig();
											SQLGestion.setDateBuyHalo(p, df.format(cal.getTime()));
											SQLGestion.setHalo(p, Double.parseDouble(code[2]));
											p.sendMessage("§a Votre nouveau halo est "+code[2]+" !");
											break;
										default:
											p.sendMessage("§cUne Erreur[TokenSwitch] s'est produite, contactez un développeur.");
											break;
									}
								}else{
									p.sendMessage("§cCe token est expiré.");
								}
							} catch (Exception e) {
								e.printStackTrace();
								p.sendMessage("§cUne Erreur[Token] s'est produite, contactez un développeur.");
							}
						}else{
							p.sendMessage("§cErreur, token non valide.");
						}
						return;
					}
				});
				p.sendMessage("§cErreur, token non valide.");
				return;
			}else{
				p.sendMessage("§cErreur, token non valide.");
				return;
			}
		}
	}

}
