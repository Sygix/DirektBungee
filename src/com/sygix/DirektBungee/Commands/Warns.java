/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import com.sygix.DirektBungee.Utils.BroadcastThor;
import com.sygix.DirektBungee.Utils.GetArgsMessage;

public class Warns extends Command {
	
	public Warns(String name) {
        super(name);
    }

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(sender instanceof ProxiedPlayer){
			if(sender.hasPermission("DirektServ.assist")){
				BroadcastThor.send(GetArgsMessage.getMessage(args, 0));
			}
		}else{
			BroadcastThor.send(GetArgsMessage.getMessage(args, 0));
		}
	}

}
