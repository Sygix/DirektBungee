/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.plugin.Command;

public class Gtp extends Command {
	
	public Gtp(String name) {
        super(name);
    }

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof ProxiedPlayer)){return;}
		ProxiedPlayer p = (ProxiedPlayer) sender;
		if((p.hasPermission("DirektServ.helper")) || (p.hasPermission("DirektServ.architecte")) || (p.hasPermission("DirektServ.developpeur"))){
			if(args.length > 0){
				final ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
				if(target == p){
					p.sendMessage(ChatColor.RED+"Nan mais t'es con ou quoi ? Tu te t�l�porte � toi m�me !");
					return;
				}
				if(target != null){
					Server s = target.getServer();
					if(p.getServer().getInfo().getName() == s.getInfo().getName()){
						p.sendMessage(ChatColor.RED+"Ce joueur est sur le m�me serveur !");
						return;
					}
					p.connect(s.getInfo());
					p.sendMessage(ChatColor.AQUA+"Vous rejoignez "+target.getName()+" sur le serveur "+s.getInfo().getName());
				}else{
					p.sendMessage(ChatColor.RED+"Le joueur n'est pas en ligne !");
					return;
				}
			}
		}

	}

}
