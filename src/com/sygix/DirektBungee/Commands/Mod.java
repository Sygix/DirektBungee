/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.plugin.Command;

import com.sygix.DirektBungee.Utils.GetArgsMessage;

public class Mod extends Command {
	
	public Mod(String name) {
        super(name);
    }

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		ProxiedPlayer p = (ProxiedPlayer) sender;
		Server s = p.getServer();
		if(p.hasPermission("DirektServ.helper")){
			final String m = GetArgsMessage.getMessage(args, 0);
			for(ProxiedPlayer pls : s.getInfo().getPlayers()){
				pls.sendMessage(ChatColor.BLUE+""+ChatColor.BOLD+"[Modération] "+p.getDisplayName()+" : "+ChatColor.RESET+""+ChatColor.GREEN+""+m);
			}
		}
	}
}
