/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class Help extends Command {
	
	public Help(String name) {
        super(name);
    }

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		ProxiedPlayer p = (ProxiedPlayer) sender;
		p.sendMessage(ChatColor.GOLD+"-----------------------------------------------------");
		p.sendMessage(ChatColor.AQUA+"/help"+ChatColor.GREEN+ChatColor.ITALIC+" : Permet d'avoir la liste des commandes.");
		p.sendMessage(ChatColor.AQUA+"/direktcoins :"+ChatColor.GREEN+ChatColor.ITALIC+" : Permet de conna�tre son solde en DirektCoins.");
		p.sendMessage(ChatColor.AQUA+"/hub"+ChatColor.GREEN+ChatColor.ITALIC+" : Permet de retourner sur le serveur principal.");
		p.sendMessage(ChatColor.AQUA+"/ping"+ChatColor.GREEN+ChatColor.ITALIC+" : Donne des informations sur la connexion et le serveur.");
		p.sendMessage(ChatColor.AQUA+"/list"+ChatColor.GREEN+ChatColor.ITALIC+" : Donne le nombre de joueurs connect� � DirektServ.");
		p.sendMessage(ChatColor.AQUA+"/parrain <pseudo>"+ChatColor.GREEN+ChatColor.ITALIC+" : Permet de d�finir votre parrain, attention ne peut pas �tre chang�. (Merci d'�crire le pseudo EXACT de votre parrain)");
		p.sendMessage(ChatColor.AQUA+"/report <pseudo> <raison>"+ChatColor.GREEN+ChatColor.ITALIC+" : Signale un joueur au staff du serveur.");
		p.sendMessage(ChatColor.AQUA+"/staff"+ChatColor.GREEN+ChatColor.ITALIC+" : Donne la liste du staff connect�.");
		p.sendMessage(ChatColor.AQUA+"/seen"+ChatColor.GREEN+ChatColor.ITALIC+" : Donne votre temps total de connexion.");
		if((p.hasPermission("DirektServ.helper")) || (p.hasPermission("DirektServ.architecte")) || (p.hasPermission("DirektServ.developpeur"))){
			p.sendMessage(ChatColor.AQUA+"!!<message>"+ChatColor.GREEN+ChatColor.ITALIC+" : Envoie un message dans le staffchat.");
			p.sendMessage(ChatColor.AQUA+"/friendgift <pseudo>"+ChatColor.GREEN+ChatColor.ITALIC+" : Permet d'offrir le grade MiniVIP le temps d'une session � un ami.");
			p.sendMessage(ChatColor.AQUA+"/gtp <pseudo>"+ChatColor.GREEN+ChatColor.ITALIC+" : Permet de se rendre sur le m�me serveur que le joueur vis�.");
			p.sendMessage(ChatColor.AQUA+"/seen <pseudo>"+ChatColor.GREEN+ChatColor.ITALIC+" : Permet de voir le temps de conexion d'une personne.");
			p.sendMessage(ChatColor.AQUA+"/cclear"+ChatColor.GREEN+ChatColor.ITALIC+" : Efface le chat.");
			p.sendMessage(ChatColor.AQUA+"/mod <message>"+ChatColor.GREEN+ChatColor.ITALIC+" : Envoie un message mod�rateur sur le serveur.");
			p.sendMessage(ChatColor.AQUA+"/<forum/site/insulte/spam/store/ts>"+ChatColor.GREEN+ChatColor.ITALIC+" : Donne les informations relatives au serveur.");
			p.sendMessage(ChatColor.AQUA+"/gkick <pseudo> <raison>"+ChatColor.GREEN+ChatColor.ITALIC+" : Kick un joueur.");
			p.sendMessage(ChatColor.AQUA+"/gtempmute <pseudo> <temps> <raison>"+ChatColor.GREEN+ChatColor.ITALIC+" : Mute un joueur.");
			p.sendMessage(ChatColor.AQUA+"/gtempban <pseudo> <temps> <raison>"+ChatColor.GREEN+ChatColor.ITALIC+" : Ban un joueur.");
		}
		p.sendMessage(ChatColor.GOLD+"-----------------------------------------------------");

	}

}
