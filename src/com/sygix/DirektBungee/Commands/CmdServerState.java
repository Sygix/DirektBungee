/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Commands;

import java.util.HashSet;
import java.util.Set;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

import com.google.common.collect.ImmutableSet;
import com.sygix.DirektBungee.DirektBungee;
import com.sygix.DirektBungee.Utils.ServerState;

public class CmdServerState extends Command implements TabExecutor {

	public CmdServerState(String name) {
        super(name);
    }

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		if(sender.hasPermission("DirektServ.admin")){
			if(args.length > 0){
				if(args[0].equalsIgnoreCase("OPEN")){
					ServerState.setState(ServerState.OPEN);
					DirektBungee.config.set("ServerState", "OPEN");
				}else if(args[0].equalsIgnoreCase("MAINTENANCE")){
					ServerState.setState(ServerState.MAINTENANCE);
					DirektBungee.config.set("ServerState", "MAINTENANCE");
				}else if(args[0].equalsIgnoreCase("BETA")){
					ServerState.setState(ServerState.BETA);
					DirektBungee.config.set("ServerState", "BETA");
				}else{
					sender.sendMessage(ChatColor.LIGHT_PURPLE+"/serverstate <open/maintenance/beta>");
					return;
				}
				sender.sendMessage(ChatColor.LIGHT_PURPLE+"Le serveur est maintenant en : "+ServerState.getState().name());
			}else{
				sender.sendMessage(ChatColor.LIGHT_PURPLE+"/serverstate <open/maintenance/beta>");
				return;
			}
		}
		DirektBungee.SaveConfig();
	}
	
	@Override
	public Iterable<String> onTabComplete(CommandSender sender, String[] args){
    	
    	if ( args.length > 1 || args.length == 0 ){
            return ImmutableSet.of();
        }
    	
        Set<String> matches = new HashSet<>();
        if ( args.length == 1 ){
            String search = args[0].toLowerCase();
            if ( "open".startsWith( search ) ) matches.add( "open" );
            if ( "maintenance".startsWith( search ) ) matches.add( "maintenance" );
            if ( "beta".startsWith( search ) ) matches.add( "beta" );
        }
        return matches;
	}

}
