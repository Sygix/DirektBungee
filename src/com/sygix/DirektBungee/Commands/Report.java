/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Commands;

import java.util.HashMap;
import java.util.UUID;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import com.sygix.DirektBungee.DirektBungee;
import com.sygix.DirektBungee.Utils.BroadcastThor;
import com.sygix.DirektBungee.Utils.GetArgsMessage;

public class Report extends Command {
	
	public static HashMap<UUID, Long> hm = new HashMap<UUID, Long>();
	
	public Report(String name) {
        super(name);
    }

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		if(args.length > 1){
            if(!(sender instanceof ProxiedPlayer)){return;}
            final ProxiedPlayer pp = (ProxiedPlayer) sender;
            if(hm.containsKey(pp.getUniqueId())){
            	long time = hm.get(pp.getUniqueId()) - System.currentTimeMillis();
            	if(!(time <= 0)){
            		long minutes = ((time / 1000) % 3600) / 60;
            		long secondes = ((time / 1000) % 3600 ) % 60;
            		pp.sendMessage(ChatColor.RED+"Vous devez attendre "+minutes+" m "+secondes+" s avant de faire un autre report.");
                	return;
            	}
            }
            final ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
        	final String message = GetArgsMessage.getMessage(args, 1);
        	if(target == null){
        		pp.sendMessage(ChatColor.RED+"Le joueur cibl� n'est pas en ligne !");
        		return;
        	}
        	hm.put(pp.getUniqueId(), (DirektBungee.rcooldown * 1000) + System.currentTimeMillis());
        	BroadcastThor.send(ChatColor.RED+""+target+ChatColor.GRAY+" a �t� signal� par "+ChatColor.GREEN+pp+ChatColor.GRAY+" pour : "+ChatColor.RED+message);
        	pp.sendMessage(ChatColor.GREEN+"Votre signalement a �t� transmis � notre �quipe.");
		}else{
			sender.sendMessage(ChatColor.RED+"Erreur, "+ChatColor.AQUA+"/report <joueur> <raison>");
		}
	}
	
}
