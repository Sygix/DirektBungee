/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Commands;

import java.sql.SQLException;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import com.sygix.DirektBungee.SQL.SQLGestion;
import com.sygix.DirektBungee.Utils.FormatTime;

public class Seen extends Command {
	
	public Seen(String name) {
        super(name);
    }

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		ProxiedPlayer p = (ProxiedPlayer) sender;
		if(args.length > 0 && args.length < 2){
			if((p.hasPermission("DirektServ.helper")) || (p.hasPermission("DirektServ.architecte")) || (p.hasPermission("DirektServ.developpeur"))){
				final String target = args[0];
				if(ProxyServer.getInstance().getPlayers().contains(target)){
					final ProxiedPlayer t = ProxyServer.getInstance().getPlayer(target);
					try {
						p.sendMessage(ChatColor.AQUA+""+t.getName()+" a pass� "+FormatTime.formatminutes(SQLGestion.getTime(target))+" sur le serveur."); //Online
					} catch (SQLException e) {
						e.printStackTrace();
						p.sendMessage(ChatColor.RED+"Une erreur[1] s'est produite, contactez Sygix !");
					} catch (Exception e) {
						p.sendMessage(ChatColor.RED+"Une erreur[2] s'est produite, contactez Sygix !");
					}
				}else{
					try {
						p.sendMessage(ChatColor.AQUA+""+target+" a pass� "+FormatTime.formatminutes(SQLGestion.getTime(target))+" sur le serveur."); //Offline
					} catch (SQLException e) {
						e.printStackTrace();
						p.sendMessage(ChatColor.RED+"Une erreur[3] s'est produite, contactez Sygix !");
					} catch (Exception e) {
						p.sendMessage(ChatColor.RED+"Une erreur[4] s'est produite, contactez Sygix !");
					}
				}
			}
		}else{
			try {
				p.sendMessage(ChatColor.AQUA+"Vous avez pass� "+FormatTime.formatminutes(SQLGestion.getTime(p))+" sur le serveur.");
			} catch (SQLException e) {
				e.printStackTrace();
				p.sendMessage(ChatColor.RED+"Une erreur[5] s'est produite, contactez Sygix !");
			}
		}
	}

}
