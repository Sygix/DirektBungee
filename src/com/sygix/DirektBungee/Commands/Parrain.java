/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Commands;

import java.sql.SQLException;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import com.sygix.DirektBungee.SQL.SQLGestion;

public class Parrain extends Command {
	
	public Parrain(String name) {
        super(name);
    }

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		ProxiedPlayer p = (ProxiedPlayer) sender;
		try {
			if(args.length != 1){
				p.sendMessage(ChatColor.AQUA+"\"/parrain <pseudo>\", merci de d'écrire le pseudo EXACT de votre parrain. Une fois votre parrain défini il ne pourra plus être changé.");
			}else{
				if(SQLGestion.getParrain(p.getName()) != null){
					p.sendMessage(ChatColor.AQUA+"Vous avez déjà un parrain");
				}else{
					if(args[0].equalsIgnoreCase(p.getName())){
						p.sendMessage(ChatColor.AQUA+"Vous ne pouvez pas être votre propore parrain.");
						return;
					}
					if(SQLGestion.getPseudo(args[0])){
						SQLGestion.setParrain(p.getName(), args[0]);
						SQLGestion.setCoins(p.getName(), SQLGestion.getCoins(p)+500);
						SQLGestion.setCoins(args[0], SQLGestion.getCoins(args[0])+100);
						p.sendMessage(ChatColor.AQUA+"Vous avez défini "+args[0]+" comme parrain, vous ne pouvez plus changer.\n"+ChatColor.YELLOW+"Parrainage : "+ChatColor.AQUA+"+500 DirektCoins\n"+ChatColor.YELLOW+"Parrain ("+args[0]+") : "+ChatColor.AQUA+"+100 DirektCoins");
					}else{
						p.sendMessage("§cCe joueur n'éxiste pas !");
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			p.sendMessage(ChatColor.RED+"Une erreur[Parrain] s'est produite contacter un d�veloppeur.");
		}
		
	}

}
