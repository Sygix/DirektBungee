/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import com.sygix.DirektBungee.DirektBungee;

public class FriendGift extends Command {
	
	public FriendGift(String name) {
        super(name);
    }

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		ProxiedPlayer p = (ProxiedPlayer) sender;
		if((p.hasPermission("DirektServ.helper")) || (p.hasPermission("DirektServ.architecte")) || (p.hasPermission("DirektServ.developpeur"))){
			if(args.length == 1){
				final ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
				if(target == null){
	        		p.sendMessage(ChatColor.RED+"Le joueur cibl� n'est pas en ligne !");
	        		return;
	        	}
				if(target == p){
	        		p.sendMessage(ChatColor.RED+"Vous ne pouvez pas vous offrir de cadeau !");
	        		return;
	        	}
				if(DirektBungee.pcnt.contains(target)){
					p.sendMessage(ChatColor.AQUA+"Ce joueur est d�j� MiniVIP !");
				}else{
					DirektBungee.pcnt.add(target);
					DirektBungee.getInstance().getProxy().getPluginManager().dispatchCommand(DirektBungee.getInstance().getProxy().getConsole(), "bp user "+target.getName()+" addgroup minivip");
					p.sendMessage(ChatColor.GREEN+"Vous venez d'offrir le MiniVIP le temps d'une session � : "+target.getDisplayName());
					target.sendMessage(ChatColor.GREEN+p.getDisplayName()+" viens de vous offrir le MiniVIP le temps d'une session.");
				}
			}else{
				p.sendMessage(ChatColor.AQUA+"\"/friendgift <pseudo>\", permet d'offrir le grade MiniVIP le temps d'une session � un ami.");
			}
		}
	}

}
/*

*/