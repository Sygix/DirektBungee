/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class Hub extends Command{
	
	public Hub(String name) {
        super(name);
    }
	
	@SuppressWarnings("deprecation")
	@Override
    public void execute(CommandSender sender, String[] args) {
		if(sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            if(p.getServer().getInfo().getName().equalsIgnoreCase("hub1") || p.getServer().getInfo().getName().equalsIgnoreCase("hub2")) {
                p.sendMessage(ChatColor.AQUA+"Tu est d�j� sur le hub !");
            } else {
                Map<String, ServerInfo> servers = ProxyServer.getInstance().getServers();
                List<ServerInfo> ss = new ArrayList<>();
                ServerInfo hub2 = null;
                for(String s : servers.keySet()) {
                    ServerInfo in = servers.get(s);
                    if(in.getName().equalsIgnoreCase("hub1") || in.getName().equalsIgnoreCase("hub2")) {
                        if(in.getName().equalsIgnoreCase("hub2")) {
                            ss.add(in);
                            hub2 = in;
                        } else {
                            ss.add(in);
                        }
                    }
                }
                for(ServerInfo si : ss) {
                    if(si.getName().equalsIgnoreCase("hub1")) {
                        if(si.getPlayers().size() >= 50) {
                            if(hub2 == null) {
                                p.connect(si);
                            } else {
                                p.connect(hub2);
                            }
                        } else {
                            p.connect(si);
                        }
                    }
                }
            }
        }
	}

}
