/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class Insulte extends Command {

	public Insulte(String name) {
        super(name);
    }

	@Override
	public void execute(CommandSender sender, String[] args) {
		
		ProxiedPlayer p = (ProxiedPlayer) sender;
		if((p.hasPermission("DirektServ.helper")) || (p.hasPermission("DirektServ.architecte")) || (p.hasPermission("DirektServ.developpeur"))){
			p.chat("&eMerci de rester poli et respectueux envers les autres joueurs !");
		}
	}

}
