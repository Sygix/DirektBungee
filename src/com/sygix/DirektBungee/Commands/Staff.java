/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class Staff extends Command {
	
	public Staff(String name) {
        super(name);
    }

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		ProxiedPlayer p = (ProxiedPlayer) sender;
		String staff = "";
		p.sendMessage(ChatColor.YELLOW+"--------------------------------------");
		p.sendMessage(ChatColor.RED+""+ChatColor.ITALIC+"Liste du staff en ligne :");
		for(ProxiedPlayer pls : ProxyServer.getInstance().getPlayers()){
			if(pls.hasPermission("DirektServ.admin")){
				staff = staff+ChatColor.DARK_RED+""+pls.getDisplayName()+", ";
			}else if(pls.hasPermission("DirektServ.assist")){
				staff = staff+ChatColor.DARK_RED+""+pls.getDisplayName()+", ";
			}else if(pls.hasPermission("DirektServ.respdev")){
				staff = staff+ChatColor.RED+""+pls.getDisplayName()+", ";
			}else if(pls.hasPermission("DirektServ.respmodo")){
				staff = staff+ChatColor.BLUE+""+pls.getDisplayName()+", ";
			}else if(pls.hasPermission("DirektServ.resparchi")){
				staff = staff+ChatColor.DARK_PURPLE+""+pls.getDisplayName()+", ";
			}else if(pls.hasPermission("DirektServ.developpeur")){
				staff = staff+ChatColor.RED+""+pls.getDisplayName()+", ";
			}else if(pls.hasPermission("DirektServ.staff")){
				staff = staff+ChatColor.DARK_GREEN+""+pls.getDisplayName()+", ";
			}else if(pls.hasPermission("DirektServ.moderateur")){
				staff = staff+ChatColor.BLUE+""+pls.getDisplayName()+", ";
			}else if(pls.hasPermission("DirektServ.architecte")){
				staff = staff+ChatColor.DARK_PURPLE+""+pls.getDisplayName()+", ";
			}else if(pls.hasPermission("DirektServ.helper")){
				staff = staff+ChatColor.DARK_AQUA+""+pls.getDisplayName()+", ";
			}
		}
		if(staff == ""){
			p.sendMessage(ChatColor.GREEN+"Aucun membre du staff en ligne :(");
		}else{
			p.sendMessage(staff);
		}
		p.sendMessage(ChatColor.YELLOW+"--------------------------------------");
	}

}
