/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Commands;

import java.sql.SQLException;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import com.sygix.DirektBungee.SQL.SQLGestion;

public class Money extends Command {

	public Money(String name) {
        super(name);
    }

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		ProxiedPlayer p = (ProxiedPlayer) sender;
		if(args.length > 0 && args.length < 2){
			if(p.hasPermission("DirektServ.resparchi") || p.hasPermission("DirektServ.respmodo") || p.hasPermission("DirektServ.respdev")){
				final String target = args[0];
				if(ProxyServer.getInstance().getPlayers().contains(target)){
					final ProxiedPlayer t = ProxyServer.getInstance().getPlayer(target);
					try {//Online
						p.sendMessage(ChatColor.YELLOW+""+t.getName()+" a : "+ChatColor.AQUA+SQLGestion.getCoins(t)+" DirektCoins (x"+SQLGestion.getHalo(t)+" Halo)");
					} catch (SQLException e) {
						e.printStackTrace();
						p.sendMessage(ChatColor.RED+"Une erreur[1] s'est produite, contactez Sygix !");
					} catch (Exception e) {
						p.sendMessage(ChatColor.RED+"Une erreur[2] s'est produite, contactez Sygix !");
					}
				}else{
					try {//Offline
						p.sendMessage(ChatColor.YELLOW+""+target+" a : "+ChatColor.AQUA+SQLGestion.getCoins(target)+" DirektCoins (x"+SQLGestion.getHalo(target)+" Halo)");
					} catch (SQLException e) {
						e.printStackTrace();
						p.sendMessage(ChatColor.RED+"Une erreur[3] s'est produite, contactez Sygix !");
					} catch (Exception e) {
						p.sendMessage(ChatColor.RED+"Une erreur[4] s'est produite, contactez Sygix !");
					}
				}
			}
		}else{
			try {
				p.sendMessage(ChatColor.YELLOW+""+p.getName()+" a : "+ChatColor.AQUA+SQLGestion.getCoins(p)+" DirektCoins (x"+SQLGestion.getHalo(p)+" Halo)");
			} catch (SQLException e) {
				e.printStackTrace();
				p.sendMessage(ChatColor.RED+"Une erreur[5] s'est produite, contactez Sygix !");
			}
		}
	}

}
