/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Commands;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

import com.google.common.collect.ImmutableSet;
import com.sygix.DirektBungee.Runnables.GestionServer;

public class ManageServer extends Command implements TabExecutor {
	
	public ManageServer(String name) {
        super(name);
    }

	@SuppressWarnings("deprecation")
    @Override
	public void execute(CommandSender sender, String[] args) {
		if(sender.hasPermission("DirektServ.assist")){
			if(args.length < 1){
				sender.sendMessage("�b/mserver <start/reload/stop/kill>");
				return;
			}else if(args[0].equalsIgnoreCase("start")){
				if(args.length < 4 ){
					sender.sendMessage("�b/mserver start <NomServeur> <sram(256M)> <xram(512M)>");
					return;
				}
				ServerInfo s = ProxyServer.getInstance().getServerInfo(args[1]);
				try {
					if(GestionServer.start(s, args[2], args[3]) == true){
						sender.sendMessage("�cLe serveur � d�marr� avec succ�s.");
					}else{
						sender.sendMessage("�cLe serveur n'as pas d�marr�, consultez les Logs.");
					}
				} catch (IOException e) {
					e.printStackTrace();
					sender.sendMessage("�cUne erreur[1] s'est produite.");
				}
			}/*else if(args[0].equalsIgnoreCase("reload")){
				if(args.length < 2 ){
					sender.sendMessage("�b/mserver reload <NomServeur>");
					return;
				}
				ServerInfo s = ProxyServer.getInstance().getServerInfo(args[1]);
				try {
					if(GestionServer.reload(s) == true){
						sender.sendMessage("�cLe serveur � �t� reload avec succ�s.");
					}else{
						sender.sendMessage("�cLe serveur n'as pas �t� reload, consultez les Logs.");
					}
				} catch (IOException e) {
					e.printStackTrace();
					sender.sendMessage("�cUne erreur[2] s'est produite.");
				}
			}else if(args[0].equalsIgnoreCase("stop")){
				if(args.length < 2 ){
					sender.sendMessage("�b/mserver stop <NomServeur>");
					return;
				}
				ServerInfo s = ProxyServer.getInstance().getServerInfo(args[1]);
				try {
					if(GestionServer.stop(s) == true){
						sender.sendMessage("�cLe serveur � �t� arr�t� avec succ�s.");
					}else{
						sender.sendMessage("�cLe serveur n'as pas �t� arr�t�, consultez les Logs.");
					}
				} catch (IOException e) {
					e.printStackTrace();
					sender.sendMessage("�cUne erreur[3] s'est produite.");
				}
			}*/else if(args[0].equalsIgnoreCase("kill")){
				if(args.length < 2 ){
					sender.sendMessage("�b/mserver kill <NomServeur>");
					return;
				}
				ServerInfo s = ProxyServer.getInstance().getServerInfo(args[1]);
				try {
					if(GestionServer.kill(s) == true){
						sender.sendMessage("�cLe serveur � �t� kill avec succ�s.");
					}else{
						sender.sendMessage("�cLe serveur n'as pas �t� kill, consultez les Logs.");
					}
				} catch (IOException e) {
					e.printStackTrace();
					sender.sendMessage("�cUne erreur[4] s'est produite.");
				}
			}else{
				sender.sendMessage("�b/mserver <start/reload/stop/kill>");
			}
		}
	}
	
    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args){
    	
    	if ( args.length > 4 || args.length == 0 ){
            return ImmutableSet.of();
        }
    	
        Set<String> matches = new HashSet<>();
        if ( args.length == 1 ){
            String search = args[0].toLowerCase();
            if ( "start".startsWith( search ) ) matches.add( "start" );
            if ( "reload".startsWith( search ) ) matches.add( "reload" );
            if ( "stop".startsWith( search ) ) matches.add( "stop" );
            if ( "kill".startsWith( search ) ) matches.add( "kill" );
        }else if ( args.length == 2 ){
            String search = args[1].toLowerCase();
            for ( String server : ProxyServer.getInstance().getServers().keySet() ){
                if ( server.toLowerCase().startsWith( search ) ){
                    matches.add( server );
                }
            }
        }else if ( args.length == 3 ){
        	String search = args[2];
            if ( "256M".startsWith( search ) ) matches.add( "256M" );
            if ( "512M".startsWith( search ) ) matches.add( "512M" );
            if ( "1024M".startsWith( search ) ) matches.add( "1024M" );
            if ( "2048M".startsWith( search ) ) matches.add( "2048M" );
        }else if ( args.length == 4 ){
        	String search = args[3];
            if ( "256M".startsWith( search ) ) matches.add( "256M" );
            if ( "512M".startsWith( search ) ) matches.add( "512M" );
            if ( "1024M".startsWith( search ) ) matches.add( "1024M" );
            if ( "2048M".startsWith( search ) ) matches.add( "2048M" );
        }
        return matches;
    }

}
