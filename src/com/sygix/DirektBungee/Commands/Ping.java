/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Commands;

import java.net.InetSocketAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class Ping extends Command {
	
	public Ping(String name) {
        super(name);
    }

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		ProxiedPlayer p = (ProxiedPlayer) sender;
		int ping = p.getPing();
		InetSocketAddress ip = p.getAddress();
		String s = p.getServer().getInfo().getName();
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy"); //HH:mm:ss
		Date date = new Date();
		int hours = date.getHours();
		int minutes = date.getMinutes();
		int sec = date.getSeconds();
		
		p.sendMessage(ChatColor.YELLOW+"--------------------------------------");
		p.sendMessage(ChatColor.YELLOW+"Date : "+ChatColor.AQUA+dateFormat.format(date));
		p.sendMessage(ChatColor.YELLOW+"Heure : "+ChatColor.AQUA+hours+":"+minutes+":"+sec);
		p.sendMessage(ChatColor.YELLOW+"Serveur : "+ChatColor.AQUA+s);
		p.sendMessage(ChatColor.YELLOW+"IP : "+ChatColor.AQUA+ip);
		p.sendMessage(ChatColor.YELLOW+"Ping : "+ChatColor.AQUA+ping+"ms");
		p.sendMessage(ChatColor.RED+""+ChatColor.ITALIC+"Plus votre ping est bas meilleure est votre connexion !");
		p.sendMessage(ChatColor.YELLOW+"--------------------------------------");
	}

}
