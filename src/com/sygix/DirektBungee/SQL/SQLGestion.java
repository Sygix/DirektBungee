/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.SQL;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import com.sygix.DirektBungee.DirektBungee;

public class SQLGestion {
	
	static Connection con = null;

    public static void connect() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        String url_ = DirektBungee.config.getString("SQL.URL");
        String pass = DirektBungee.config.getString("SQL.Pass");
        String user = DirektBungee.config.getString("SQL.User");

        con = DriverManager.getConnection(url_, user, pass);
    }
    
    public static void disconnect() throws SQLException {
    	if(con != null){
    		con.close();
    	}
    }
    
    @SuppressWarnings("unused")
    public static void registerPlugin() throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        try {
            ResultSet res = state.executeQuery("SELECT * FROM players");
        } catch(SQLException e) {
            state.execute("CREATE TABLE players ( id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, uid VARCHAR(100), pseudo VARCHAR(100), grade VARCHAR(100), time LONG, lastconnect LONG, coins DOUBLE, halo DOUBLE, date_buy_halo VARCHAR(100), date_buy_grade VARCHAR(100), parrain VARCHAR(100))");
        }
        try{
        	ResultSet res = state.executeQuery("SELECT * FROM servers");
        }catch(SQLException e){
        	state.execute("CREATE TABLE servers ( id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, serveur VARCHAR(100), state VARCHAR(100), joueurs INT, maintenance BOOLEAN)");
        }
        state.close();
    }
    
    public static boolean registerOnBDD(ProxiedPlayer p) throws SQLException, IOException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE uid='" + p.getUniqueId()+ "'");
        while(!res.next()){
        	Statement s = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            s.executeUpdate("INSERT INTO players (id, uid, pseudo, grade, time, lastconnect, coins, halo, date_buy_halo, date_buy_grade, parrain, ) VALUES (null, '"+p.getUniqueId()+"', '"+p.getName()+"', null, 0, 0, 0.0, 1.0, null, null, null)");
            state.close();
            return false;
        }
        state.close();
        return true;
    }
    
    public static void setTime(ProxiedPlayer p, long time) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        state.executeUpdate("UPDATE players SET time="+time+" WHERE uid='"+p.getUniqueId()+"'");
        state.close();
    }
    
    public static long getTime(ProxiedPlayer p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE uid='"+p.getUniqueId()+"'");
        while(res.next()) {
            long time = res.getLong("time");
            state.close();
            return time;
        }
        state.close();
        return 0;
    }
    
    public static long getTime(String p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE pseudo='"+p+"'");
        while(res.next()) {
            long time = res.getLong("time");
            state.close();
            return time;
        }
        state.close();
        return 0;
    }
    
    public static void setlastconnect(ProxiedPlayer p, long time) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        state.executeUpdate("UPDATE players SET lastconnect="+time+" WHERE uid='"+p.getUniqueId()+"'");
        state.close();
    }
    
    public static long getlastconnect(ProxiedPlayer p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE uid='"+p.getUniqueId()+"'");
        while(res.next()) {
            long lastconnect = res.getLong("lastconnect");
            state.close();
            return lastconnect;
        }
        state.close();
        return 0;
    }
    
    public static String getPseudo(ProxiedPlayer p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE uid='"+p.getUniqueId()+"'");
        while(res.next()) {
            String pseudo = res.getString("pseudo");
            state.close();
            return pseudo;
        }
        state.close();
        return "Erreur, Aucun pseudo.";
    }
    
    public static boolean getPseudo(String p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE pseudo='"+p+"'");
        while(res.next()) {
            state.close();
            return true;
        }
        state.close();
        return false;
    }
    
    public static void setPseudo(ProxiedPlayer p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        state.executeUpdate("UPDATE players SET pseudo='"+p.getName()+"' WHERE uid='"+p.getUniqueId()+"'");
        state.close();
    }

    public static String getGrade(ProxiedPlayer p) throws SQLException {
        if(!con.isValid(2)){
            try {
                connect();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE uid='"+p.getUniqueId()+"'");
        while(res.next()) {
            String grade = res.getString("grade");
            state.close();
            return grade;
        }
        state.close();
        return "Erreur, Aucun grade.";
    }

    public static String getGrade(String p) throws SQLException {
        if(!con.isValid(2)){
            try {
                connect();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE pseudo='"+p+"'");
        while(res.next()) {
            String grade = res.getString("grade");
            state.close();
            return grade;
        }
        state.close();
        return "Erreur, Aucun grade.";
    }

    public static void setGrade(ProxiedPlayer p, String grade) throws SQLException {
        if(!con.isValid(2)){
            try {
                connect();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        state.executeUpdate("UPDATE players SET grade='"+grade+"' WHERE uid='"+p.getUniqueId()+"'");
        state.close();
    }
    
    public static void setHalo(ProxiedPlayer p, double halo) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        state.executeUpdate("UPDATE players SET halo="+halo+" WHERE uid='"+p.getUniqueId()+"'");
        state.close();
    }
    
    public static double getHalo(ProxiedPlayer p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE uid='"+p.getUniqueId()+"'");
        while(res.next()) {
            double halo = res.getDouble("halo");
            state.close();
            return halo;
        }
        state.close();
        return 0.0;
    }
    
    public static double getHalo(String p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE pseudo='"+p+"'");
        while(res.next()) {
            double halo = res.getDouble("halo");
            state.close();
            return halo;
        }
        state.close();
        return 0.0;
    }
    
    public static void setDateBuyHalo(ProxiedPlayer p, String date) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        state.executeUpdate("UPDATE players SET date_buy_halo="+date+" WHERE uid='"+p.getUniqueId()+"'");
        state.close();
    }
    
    public static String getDateBuyHalo(ProxiedPlayer p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE uid='"+p.getUniqueId()+"'");
        while(res.next()) {
        	String date = res.getString("date_buy_halo");
            state.close();
            return date;
        }
        state.close();
        return null;
    }

    public static void setDateBuyGrade(ProxiedPlayer p, String date) throws SQLException {
        if(!con.isValid(2)){
            try {
                connect();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        state.executeUpdate("UPDATE players SET date_buy_grade="+date+" WHERE uid='"+p.getUniqueId()+"'");
        state.close();
    }

    public static String getDateBuyGrade(ProxiedPlayer p) throws SQLException {
        if(!con.isValid(2)){
            try {
                connect();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE uid='"+p.getUniqueId()+"'");
        while(res.next()) {
            String date = res.getString("date_buy_grade");
            state.close();
            return date;
        }
        state.close();
        return null;
    }
    
    public static void setCoins(ProxiedPlayer p, double coins) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        state.executeUpdate("UPDATE players SET coins="+coins+" WHERE uid='"+p.getUniqueId()+"'");
        state.close();
    }
    
    public static void setCoins(String p, double coins) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        state.executeUpdate("UPDATE players SET coins="+coins+" WHERE pseudo='"+p+"'");
        state.close();
    }
    
    public static double getCoins(ProxiedPlayer p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE uid='"+p.getUniqueId()+"'");
        while(res.next()) {
            double coins = res.getDouble("coins");
            state.close();
            return coins;
        }
        state.close();
        return 0.0;
    }
    
    public static double getCoins(String p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE pseudo='"+p+"'");
        while(res.next()) {
            double coins = res.getDouble("coins");
            state.close();
            return coins;
        }
        state.close();
        return 0.0;
    }
    
    public static String getParrain(String p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE pseudo='"+p+"'");
        while(res.next()) {
            String parrain = res.getString("parrain");
            state.close();
            return parrain;
        }
        state.close();
        return "Erreur, Aucun parrain.";
    }
    
    public static void setParrain(String p, String parrain) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        state.executeUpdate("UPDATE players SET parrain='"+parrain+"' WHERE pseudo='"+p+"'");
        state.close();
    }
    
    //Serveurs
    
    public static boolean registerServerOnBDD(ServerInfo server) throws SQLException, IOException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM servers WHERE serveur='" + server.getName() + "'");
        while(!res.next()){
        	Statement s = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            s.executeUpdate("INSERT INTO servers (id, serveur, state, joueurs, maintenance) VALUES (null, '"+server.getName()+"', null, 0, 0)");
            state.close();
            return false;
        }
        state.close();
        return true;
    }
    
    public static void setState(ServerInfo serveur, String State) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        state.executeUpdate("UPDATE servers SET state='"+State+"' WHERE serveur='"+serveur.getName()+"'");
        state.close();
    }
    
    public static String getState(ServerInfo serveur) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM servers WHERE serveur='"+serveur.getName()+"'");
        while(res.next()) {
            String State = res.getString("state");
            state.close();
            return State;
        }
        state.close();
        return null;
    }
    
    public static void setJoueurs(ServerInfo serveur, int nbr) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        state.executeUpdate("UPDATE servers SET joueurs="+nbr+" WHERE serveur='"+serveur.getName()+"'");
        state.close();
    }
    
    public static int getJoueurs(ServerInfo serveur) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM servers WHERE serveur='"+serveur.getName()+"'");
        while(res.next()) {
            int nbr = res.getInt("joueurs");
            state.close();
            return nbr;
        }
        state.close();
        return 1812;
    }

}
