/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.scheduler.ScheduledTask;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import com.sygix.DirektBungee.Commands.Aide;
import com.sygix.DirektBungee.Commands.ChatClear;
import com.sygix.DirektBungee.Commands.CmdServerState;
import com.sygix.DirektBungee.Commands.Coins;
import com.sygix.DirektBungee.Commands.DirektCoins;
import com.sygix.DirektBungee.Commands.Forum;
import com.sygix.DirektBungee.Commands.FriendGift;
import com.sygix.DirektBungee.Commands.Gtp;
import com.sygix.DirektBungee.Commands.Help;
import com.sygix.DirektBungee.Commands.Hub;
import com.sygix.DirektBungee.Commands.Insulte;
import com.sygix.DirektBungee.Commands.Lag;
import com.sygix.DirektBungee.Commands.List;
import com.sygix.DirektBungee.Commands.Lobby;
import com.sygix.DirektBungee.Commands.Ls;
import com.sygix.DirektBungee.Commands.ManageServer;
import com.sygix.DirektBungee.Commands.Mod;
import com.sygix.DirektBungee.Commands.Money;
import com.sygix.DirektBungee.Commands.Parrain;
import com.sygix.DirektBungee.Commands.Ping;
import com.sygix.DirektBungee.Commands.Reload;
import com.sygix.DirektBungee.Commands.Report;
import com.sygix.DirektBungee.Commands.Seen;
import com.sygix.DirektBungee.Commands.Site;
import com.sygix.DirektBungee.Commands.Spam;
import com.sygix.DirektBungee.Commands.Staff;
import com.sygix.DirektBungee.Commands.Store;
import com.sygix.DirektBungee.Commands.TeamSpeak;
import com.sygix.DirektBungee.Commands.Token;
import com.sygix.DirektBungee.Commands.Warns;
import com.sygix.DirektBungee.Discord.MessageListener;
import com.sygix.DirektBungee.Events.Chat;
import com.sygix.DirektBungee.Events.PlayerJoin;
import com.sygix.DirektBungee.Events.PlayerKicked;
import com.sygix.DirektBungee.Events.PlayerQuit;
import com.sygix.DirektBungee.Events.PluginMessagingEvent;
import com.sygix.DirektBungee.Events.StaffChat;
import com.sygix.DirektBungee.Events.TabComplete;
import com.sygix.DirektBungee.Events.Votifier;
import com.sygix.DirektBungee.Runnables.MainTask;
import com.sygix.DirektBungee.SQL.SQLGestion;
import com.sygix.DirektBungee.Utils.ServerState;

public class DirektBungee extends Plugin{
	
	public static DirektBungee instance;
	public static ArrayList<ProxiedPlayer> pcnt = new ArrayList<>();
	public static ScheduledTask rcooldowntask;
	public static boolean rcooldownrun = false;
	public static long rcooldown = 900;
	public static DirektBungee getInstance(){
		return instance;
	}
	public static Configuration config;
	public static Configuration token;
	
	public void onEnable(){
		instance = this;
		try{
			if(!this.getDataFolder().exists()){
				this.getDataFolder().mkdir();
			}
			File file = new File(this.getDataFolder().getPath(), "config.yml");
			File filetoken = new File(this.getDataFolder().getPath(), "token.yml");
			if(!file.exists()){
				ArrayList<String> annonces = new ArrayList<>();
				annonces.add("&c[Annonce] &eDirektServ possède une boutique, achetez votre grade ici : &bhttp://direktserv.fr/boutique.html");
				annonces.add("&c[Annonce] &eVous pouvez contacter notre staff oralement sur TeamSpeak ! &bts.direktserv.fr");
				annonces.add("&c[Annonce] &eN'hesitez pas à aller faire un tour sur notre Forum : &bhttp://forum.direktserv.fr");
				annonces.add("&c[Annonce] &eNotre site web est disponible à l'adresse : &bhttps://www.direktserv.fr");
				annonces.add("&c[Annonce] &eSuivez-nous sur Twitter : &bhttp://twitter.com/DirektServ");
				file.createNewFile();
				config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
				config.set("SQL.User", "root");
				config.set("SQL.Pass", "****");
				config.set("SQL.URL", "jdbc:mysql://mc.direktserv.fr/Bungee");
				config.set("ServerState", "OPEN");
				config.set("Annonces", annonces);
				config.set("MOTD.OPEN", "&a&k!i&r &c&lDirektServ &7- &c&lV2 &r&a&k!i&r &6Serveur de jeux &l&c1.10 &r&6!            &e>>> OUVERT !");
				config.set("MOTD.BETA", "&a&k!i&r &c&lDirektServ &7- &c&lV2 &r&a&k!i&r &6Serveur de jeux &l&c1.10 &r&6!            &e>>> BETA !");
				config.set("MOTD.MAINTENANCE", "&a&k!i&r &c&lDirektServ &7- &c&lV2 &r&a&k!i&r &6Serveur de jeux &l&c1.10 &r&6!            &e>>> MAINTENANCE !");
				config.set("Slow", "3");
				ConfigurationProvider.getProvider(YamlConfiguration.class).save(config, file);
			}else{
				config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
			}
			if(!filetoken.exists()){
				filetoken.createNewFile();
				token = ConfigurationProvider.getProvider(YamlConfiguration.class).load(filetoken);
				token.set("Tokens.C-000-000-15.Expiry", "01/01/1970");
				ConfigurationProvider.getProvider(YamlConfiguration.class).save(token, filetoken);
			}else{
				token = ConfigurationProvider.getProvider(YamlConfiguration.class).load(filetoken);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		if(config.get("ServerState").equals("OPEN")){
			ServerState.setState(ServerState.OPEN);
		}else if(config.get("ServerState").equals("BETA")){
			ServerState.setState(ServerState.BETA);
		}else if(config.get("ServerState").equals("MAINTENANCE")){
			ServerState.setState(ServerState.MAINTENANCE);
		}
		
		getProxy().getPluginManager().registerListener(this, new PlayerKicked());
		getProxy().getPluginManager().registerListener(this, new StaffChat());
		getProxy().getPluginManager().registerListener(this, new PlayerJoin());
		getProxy().getPluginManager().registerListener(this, new PlayerQuit());
		getProxy().getPluginManager().registerListener(this, new TabComplete());
		getProxy().getPluginManager().registerListener(this, new Votifier());
		getProxy().getPluginManager().registerListener(this, new Chat());
		getProxy().getPluginManager().registerListener(this, new PluginMessagingEvent());

		getProxy().getPluginManager().registerCommand(this, new Hub("hub"));
		getProxy().getPluginManager().registerCommand(this, new Lobby("lobby"));
		getProxy().getPluginManager().registerCommand(this, new Lag("lag"));
		getProxy().getPluginManager().registerCommand(this, new Ping("ping"));
		getProxy().getPluginManager().registerCommand(this, new Staff("staff"));
		getProxy().getPluginManager().registerCommand(this, new Report("report"));
		getProxy().getPluginManager().registerCommand(this, new List("list"));
		getProxy().getPluginManager().registerCommand(this, new Ls("ls"));
		getProxy().getPluginManager().registerCommand(this, new Mod("mod"));
		getProxy().getPluginManager().registerCommand(this, new ChatClear("cclear"));
		getProxy().getPluginManager().registerCommand(this, new Seen("seen"));
		getProxy().getPluginManager().registerCommand(this, new TeamSpeak("ts"));
		getProxy().getPluginManager().registerCommand(this, new Store("store"));
		getProxy().getPluginManager().registerCommand(this, new Spam("spam"));
		getProxy().getPluginManager().registerCommand(this, new Insulte("insulte"));
		getProxy().getPluginManager().registerCommand(this, new Site("site"));
		getProxy().getPluginManager().registerCommand(this, new Forum("forum"));
		getProxy().getPluginManager().registerCommand(this, new Coins("coins"));
		getProxy().getPluginManager().registerCommand(this, new DirektCoins("direktcoins"));
		getProxy().getPluginManager().registerCommand(this, new Money("money"));
		getProxy().getPluginManager().registerCommand(this, new Gtp("gtp"));
		getProxy().getPluginManager().registerCommand(this, new Warns("warns"));
		getProxy().getPluginManager().registerCommand(this, new Parrain("parrain"));
		getProxy().getPluginManager().registerCommand(this, new FriendGift("friendgift"));
		getProxy().getPluginManager().registerCommand(this, new Help("help"));
		getProxy().getPluginManager().registerCommand(this, new Aide("aide"));
		getProxy().getPluginManager().registerCommand(this, new CmdServerState("serverstate"));
		getProxy().getPluginManager().registerCommand(this, new Reload("dbreload"));
		getProxy().getPluginManager().registerCommand(this, new ManageServer("mserver"));
		getProxy().getPluginManager().registerCommand(this, new Token("token"));
		
		getProxy().registerChannel("DirektBungee");
		
        try {
			SQLGestion.connect();
			SQLGestion.registerPlugin();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
        
        MainTask.checkServers();
        MainTask.Annonce();
        
        MessageListener.main(null);
        
		getLogger().info("----DIREKTGESTION----\n"+"Version : "+getDescription().getVersion()+"\n"+ServerState.getState().name()+"\nEtat : Charge");
	}
	
	public void onDisable(){
		try {
			SQLGestion.disconnect();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		getLogger().info("----DIREKTGESTION----\n"+"Version : "+getDescription().getVersion()+"\n"+"Etat : Decharge");
	}
	
	public static void SaveConfig(){
		File file = new File(DirektBungee.getInstance().getDataFolder().getPath(), "config.yml");
		File tokenfile = new File(DirektBungee.getInstance().getDataFolder().getPath(), "token.yml");
		try {
			ConfigurationProvider.getProvider(YamlConfiguration.class).save(config, file);
			ConfigurationProvider.getProvider(YamlConfiguration.class).save(token, tokenfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
