/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Runnables;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.scheduler.ScheduledTask;

import com.sygix.DirektBungee.DirektBungee;
import com.sygix.DirektBungee.SQL.SQLGestion;
import com.sygix.DirektBungee.Utils.GetArgsMessage;
import com.sygix.DirektBungee.Utils.SetRandom;

public class MainTask {
	
	public static ScheduledTask annoncetask;
	private static boolean annoncerun = false;
	
	public static ScheduledTask checkstask;
	private static boolean checksrun = false;
	
	@SuppressWarnings("unchecked")
	static ArrayList<String> a = (ArrayList<String>) DirektBungee.config.get("Annonces");
	
	public static void Annonce(){
		if(annoncerun == true){
			ProxyServer.getInstance().getScheduler().cancel(annoncetask);
			return;
		}
		annoncerun = true;
		annoncetask = DirektBungee.instance.getProxy().getScheduler().schedule(DirektBungee.getInstance(), new Runnable() {
            @SuppressWarnings("deprecation")
			@Override
            public void run() {            	
                int r = SetRandom.getRandom(0, a.size()-1);
                for(ProxiedPlayer pls : ProxyServer.getInstance().getPlayers()){
                	pls.sendMessage(GetArgsMessage.fixColors(a.get(r)));
                }
            }
        }, 1, 5, TimeUnit.MINUTES);
	}
	
	public static void checkServers(){
		if(checksrun == true){
			ProxyServer.getInstance().getScheduler().cancel(checkstask);
			return;
		}
		checksrun = true;
		checkstask = DirektBungee.instance.getProxy().getScheduler().schedule(DirektBungee.getInstance(), new Runnable() {
            @Override
            public void run() {            	
            	for(ServerInfo s : ProxyServer.getInstance().getServers().values()){
                	if(s.getName().contains("CoreRush") || s.getName().contains("Arcade")){
                		try {
                			String state = SQLGestion.getState(s);
                			if(!state.equalsIgnoreCase("DOWN") && !state.equalsIgnoreCase("BOOTING") && !state.equalsIgnoreCase("MAINTENANCE")){
                				if(SQLGestion.getJoueurs(s) <= 0){
                					GestionServer.kill(s);
                				}else if(SQLGestion.getJoueurs(s) == 1812){
                					System.out.println("ERREUR GET JOUEURS BDD CHECKSERVER");
                				}
                			}
    					} catch (SQLException | IOException e) {
    						e.printStackTrace();
    					}
                	}
                }
            }
        }, 1, 15, TimeUnit.MINUTES);
	}

}
