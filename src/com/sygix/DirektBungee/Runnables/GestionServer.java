/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Runnables;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;

import com.sygix.DirektBungee.SQL.SQLGestion;

import net.md_5.bungee.api.config.ServerInfo;

public class GestionServer {
	
	public static boolean start(ServerInfo s, String sram, String xram) throws IOException {

	    File tempScript = createStartTempScript(s, sram, xram);

	    try {
	        ProcessBuilder pb = new ProcessBuilder("bash", tempScript.toString());
	        pb.inheritIO();
	        Process process = pb.start();
	        process.waitFor();
	        SQLGestion.registerServerOnBDD(s);
	        SQLGestion.setState(s, "BOOTING");
	        return true;
	    } catch (Exception e){
	    	e.printStackTrace();
	    	return false;
	    } finally {
	        tempScript.delete();
	    }
	}
	
	public static boolean reload(ServerInfo s) throws IOException{

	    File tempScript = createCommandTempScript(s, "reload");

	    try {
	        ProcessBuilder pb = new ProcessBuilder("bash", tempScript.toString());
	        pb.inheritIO();
	        Process process = pb.start();
	        process.waitFor();
	        SQLGestion.setState(s, "BOOTING");
	        return true;
	    } catch (Exception e){
	    	e.printStackTrace();
	    	return false;
	    } finally {
	        tempScript.delete();
	    }
	}
	
	public static boolean stop(ServerInfo s) throws IOException{

	    File tempScript = createCommandTempScript(s, "stop");

	    try {
	        ProcessBuilder pb = new ProcessBuilder("bash", tempScript.toString());
	        pb.inheritIO();
	        Process process = pb.start();
	        process.waitFor();
	        SQLGestion.setState(s, "DOWN");
	        return true;
	    } catch (Exception e){
	    	e.printStackTrace();
	    	return false;
	    } finally {
	        tempScript.delete();
	    }
	}
	
	public static boolean kill(ServerInfo s) throws IOException {

	    File tempScript = createKillTempScript(s);

	    try {
	        ProcessBuilder pb = new ProcessBuilder("bash", tempScript.toString());
	        pb.inheritIO();
	        Process process = pb.start();
	        process.waitFor();
	        SQLGestion.setState(s, "DOWN");
	        return true;
	    } catch (Exception e){
	    	e.printStackTrace();
	    	return false;
	    } finally {
	        tempScript.delete();
	    }
	}
	
	private static File createStartTempScript(ServerInfo s, String sram, String xram) throws IOException {
	    File tempScript = File.createTempFile("script", null);
	    Writer streamWriter = new OutputStreamWriter(new FileOutputStream(tempScript));
	    PrintWriter printWriter = new PrintWriter(streamWriter);
	    printWriter.println("#!/bin/bash");
	    printWriter.println("cd /var/games/minecraft/servers/4_"+s.getName()+"/");
	    printWriter.println("screen -dm -S mc-4_"+s.getName()+" java -Xms"+sram+" -Xmx"+xram+" -jar spigot-latest.jar");
	    printWriter.close();
	    return tempScript;
	}
	
	private static File createCommandTempScript(ServerInfo s, String cmd) throws IOException {
	    File tempScript = File.createTempFile("script", null);
	    Writer streamWriter = new OutputStreamWriter(new FileOutputStream(tempScript));
	    PrintWriter printWriter = new PrintWriter(streamWriter);
	    printWriter.println("#!/bin/bash");
	    printWriter.println("screen -R mc-4_"+s.getName()+" -p 0 -X stuff "+cmd+"$(printf \\r)");
	    printWriter.close();
	    return tempScript;
	}
	
	private static File createKillTempScript(ServerInfo s) throws IOException {
	    File tempScript = File.createTempFile("script", null);
	    Writer streamWriter = new OutputStreamWriter(new FileOutputStream(tempScript));
	    PrintWriter printWriter = new PrintWriter(streamWriter);
	    printWriter.println("#!/bin/bash");
	    printWriter.println("screen -X -S mc-4_"+s.getName()+" kill");
	    printWriter.close();
	    return tempScript;
	}

}
