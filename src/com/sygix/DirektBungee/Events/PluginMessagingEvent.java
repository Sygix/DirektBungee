package com.sygix.DirektBungee.Events;

import java.io.IOException;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import com.sygix.DirektBungee.Runnables.GestionServer;

public class PluginMessagingEvent implements Listener {

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onReceiveMessage(PluginMessageEvent e) {
		if(!e.getTag().equalsIgnoreCase("DirektBungee")) return;
		ByteArrayDataInput in = ByteStreams.newDataInput(e.getData());
		String sub = in.readUTF();
		if(sub.equalsIgnoreCase("serveurs")) {
			String server = in.readUTF();
			try {
				ServerInfo s = ProxyServer.getInstance().getServerInfo(server);
				if(GestionServer.start(s, "256M", "512M")){
					ProxyServer.getInstance().getPlayer(in.readUTF()).sendMessage("�aLe serveur "+s.getName()+" est en train de d�marrer, vous avez �t� plac� dans la file d'attente.");
				} else {
					ProxyServer.getInstance().getPlayer(in.readUTF()).sendMessage("�cLe serveur "+s.getName()+" n'a pas d�marr�, une erreur s'est produite.");
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
}
