/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Events;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.dv8tion.jda.core.JDA.Status;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import com.sygix.DirektBungee.DirektBungee;
import com.sygix.DirektBungee.Discord.MessageListener;
import com.sygix.DirektBungee.Discord.SendMessage;

public class Chat implements Listener {
	
	static HashMap<ProxiedPlayer, Long> hm = new HashMap<ProxiedPlayer, Long>();
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerChat(ChatEvent e){
		ProxiedPlayer p = (ProxiedPlayer) e.getSender();
		String m = e.getMessage();
		String car = "!!";
		Pattern pattern = Pattern.compile("(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])");
        Matcher matcher = pattern.matcher(m);
		if(m.startsWith(car))return;
		if(p.hasPermission("DirektServ.assist"))return;
		if(hm.containsKey(p)){
        	long time = hm.get(p) - System.currentTimeMillis();
        	if(!(time <= 0)){
        		long secondes = ((time / 1000) % 3600 ) % 60;
        		p.sendMessage("§cVous parlez trop vite, veuillez attendre §a"+secondes+"§cs.");
        		e.setCancelled(true);
            	return;
        	}
        }
        if(matcher.find()){
        	p.sendMessage("§cLa publicitée/IP est interdite sur le serveur.");
            e.setCancelled(true);
            return;
        }
		hm.put(p, (DirektBungee.config.getInt("Slow") * 1000) + System.currentTimeMillis());
		if(!m.startsWith("/")){
			if(MessageListener.jda.getStatus().equals(Status.CONNECTED)){
				SendMessage.sendToDiscord(MessageListener.jda, m, p.getDisplayName(), p.getServer().getInfo().getName());
			}
		}
	}

}
