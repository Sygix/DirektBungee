/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Events;

import java.sql.SQLException;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import com.sygix.DirektBungee.SQL.SQLGestion;
import com.vexsoftware.votifier.bungee.events.VotifierEvent;

public class Votifier implements Listener {
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onVoteEvent(VotifierEvent e){
		if(ProxyServer.getInstance().getPlayer(e.getVote().getUsername()) != null){
			final ProxiedPlayer target = ProxyServer.getInstance().getPlayer(e.getVote().getUsername());
			try {
				SQLGestion.setCoins(target, SQLGestion.getCoins(target)+10);
			} catch (SQLException e1) {
				e1.printStackTrace();
				target.sendMessage("�cUne erreur[Vote1], s'est produite contactez un d�veloppeur.");
			}
			target.sendMessage("�bVous venez de voter pour DirektServ sur : �a"+e.getVote().getServiceName()+" �b, merci de votre vote ! +10 DirektCoins");
		}
	}

}
