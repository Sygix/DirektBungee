/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Events;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class StaffChat implements Listener {
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onStaffChat(ChatEvent e){
		String m = e.getMessage();
		String car = "!!";
		ProxiedPlayer p = (ProxiedPlayer) e.getSender();
		if((p.hasPermission("DirektServ.admin")) || 
				(p.hasPermission("DirektServ.assist")) || 
				(p.hasPermission("DirektServ.respdev")) ||
				(p.hasPermission("DirektServ.respmodo")) ||
				(p.hasPermission("DirektServ.resparchi")) ||
				(p.hasPermission("DirektServ.developpeur")) ||
				(p.hasPermission("DirektServ.staff")) ||
				(p.hasPermission("DirektServ.moderateur")) ||
				(p.hasPermission("DirektServ.architecte")) ||
				(p.hasPermission("DirektServ.helper"))){
			if(m.startsWith(car)){
				e.setCancelled(true);
				m = m.replaceFirst(car, "");
				for(ProxiedPlayer pls : ProxyServer.getInstance().getPlayers()){
					if((pls.hasPermission("DirektServ.admin")) || 
						(pls.hasPermission("DirektServ.assist")) || 
						(pls.hasPermission("DirektServ.respdev")) ||
						(pls.hasPermission("DirektServ.respmodo")) ||
						(pls.hasPermission("DirektServ.resparchi")) ||
						(pls.hasPermission("DirektServ.developpeur")) ||
						(pls.hasPermission("DirektServ.staff")) ||
						(pls.hasPermission("DirektServ.moderateur")) ||
						(pls.hasPermission("DirektServ.architecte")) ||
						(pls.hasPermission("DirektServ.helper"))){
						pls.sendMessage(ChatColor.GOLD+"[StaffChat] "+ChatColor.RED+p.getDisplayName()+" : "+ChatColor.GREEN+m);
					}
				}
			}
		}
	}

}
