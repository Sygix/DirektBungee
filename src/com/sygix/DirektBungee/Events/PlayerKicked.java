/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Events;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PlayerKicked implements Listener{
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onServerKick(ServerKickEvent e){
		ProxiedPlayer p = e.getPlayer();
		e.setCancelled(true);
		p.sendMessage(e.getKickReason());
	}

}
