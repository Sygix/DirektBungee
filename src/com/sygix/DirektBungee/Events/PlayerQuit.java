/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Events;

import java.sql.SQLException;

import net.dv8tion.jda.core.JDA.Status;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import com.sygix.DirektBungee.DirektBungee;
import com.sygix.DirektBungee.Discord.MessageListener;
import com.sygix.DirektBungee.Discord.SendMessage;
import com.sygix.DirektBungee.Runnables.MainTask;
import com.sygix.DirektBungee.SQL.SQLGestion;
import com.sygix.DirektBungee.Utils.TabList;

public class PlayerQuit implements Listener {
	
	@EventHandler
	public void onPlayerQuit(PlayerDisconnectEvent e){
		ProxiedPlayer p = e.getPlayer();
		TabList.updatetablist(ProxyServer.getInstance().getOnlineCount()-1);
		try {
			long lastconnect = (SQLGestion.getlastconnect(p)- 1)+ System.currentTimeMillis();
			long minutes = ((lastconnect / 1000) % 3600) / 60;
			long time = SQLGestion.getTime(p) + minutes;
			SQLGestion.setTime(p, time);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		if(DirektBungee.pcnt.contains(p)){
			DirektBungee.pcnt.remove(p);
			DirektBungee.getInstance().getProxy().getPluginManager().dispatchCommand(DirektBungee.getInstance().getProxy().getConsole(), "bp user "+p.getName()+" removegroup minivip");
		}
		if(ProxyServer.getInstance().getOnlineCount()-1 <= 0){
			MainTask.Annonce();
		}
		if(MessageListener.jda.getStatus().equals(Status.CONNECTED)){
			SendMessage.sendToDiscord(MessageListener.jda, "s'est déconnecté.", p.getDisplayName(), "Bungee");
		}
	}

}
