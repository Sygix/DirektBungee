/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Events;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.TabCompleteEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

public class TabComplete implements Listener {
	
	@EventHandler(priority = EventPriority.HIGH)
    public void onTab(TabCompleteEvent e) {
		if (!e.getSuggestions().isEmpty()) {
            return; //si il y a une suggestion par un autre plugin, ne rien faire
        }
        String[] args = e.getCursor().split(" ");
        final String checked = (args.length > 0 ? args[args.length - 1] : e.getCursor()).toLowerCase();
        for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
            if (player.getName().toLowerCase().startsWith(checked)) {
                e.getSuggestions().add(player.getName());
            }
        }
	}

}
