/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Events;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import net.dv8tion.jda.core.JDA.Status;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import com.sygix.DirektBungee.DirektBungee;
import com.sygix.DirektBungee.Discord.MessageListener;
import com.sygix.DirektBungee.Discord.SendMessage;
import com.sygix.DirektBungee.Runnables.MainTask;
import com.sygix.DirektBungee.SQL.SQLGestion;
import com.sygix.DirektBungee.Utils.GetArgsMessage;
import com.sygix.DirektBungee.Utils.ServerState;
import com.sygix.DirektBungee.Utils.TabList;

public class PlayerJoin implements Listener {
	
	@SuppressWarnings("deprecation")
	@EventHandler
    public void onPing(ProxyPingEvent e){	
		if(e.getResponse().getVersion().getProtocol() < 107){
			e.getResponse().setVersion(new ServerPing.Protocol( "DirektServ 1.9 -> 1.11.X", 999));
		}
		if(ServerState.getState().canJoin() == false){
			e.getResponse().setVersion(new ServerPing.Protocol( ServerState.getState().name()+"", 999));
			if(ServerState.isState(ServerState.BETA)){
				e.getResponse().setDescription(GetArgsMessage.fixColors(DirektBungee.config.getString("MOTD.BETA")));
			}else if(ServerState.isState(ServerState.MAINTENANCE)){
				e.getResponse().setDescription(GetArgsMessage.fixColors(DirektBungee.config.getString("MOTD.MAINTENANCE")));
			}
			return;
		}
		e.getResponse().setDescription(GetArgsMessage.fixColors(DirektBungee.config.getString("MOTD.OPEN")));
    }
	
	@EventHandler
    public void onPlayerPreJoin(PreLoginEvent e){
		if(e.getConnection().getVersion() < 107){
			e.setCancelReason(ChatColor.RED+""+ChatColor.BOLD+"Merci d'utiliser une version de minecraft supérieure, versions compatible : 1.9.X|1.10.X|1.11.X !");
			e.setCancelled(true);
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerJoin(PostLoginEvent e){
		ProxiedPlayer p = e.getPlayer();
		if(ServerState.getState().canJoin() == false){
			if((p.hasPermission("DirektServ.helper")) || (p.hasPermission("DirektServ.architecte")) || (p.hasPermission("DirektServ.developpeur"))){
			}else{
				p.disconnect("§cLe serveur est en "+ServerState.getState().name()+"\n \n+d'infos sur notre Twitter : §a@DirektServ\n \n§cOu sur notre site web : \n§ahttps://www.direktserv.fr/");
				return;
			}
		}
		if((p.getName().contains("Erisium") || p.getName().contains("Epicube") || p.getName().contains("SamaGames")) && !p.getName().contains("Energy45")){
			p.disconnect("§cVotre pseudo contient de la pub, merci de le changer pour vous connecter.\n \n+d'infos sur notre Twitter : §a@DirektServ\n \n§cOu sur notre site web : \n§ahttps://www.direktserv.fr/");
			return;
		}
		TabList.updatetablist(ProxyServer.getInstance().getOnlineCount());
		p.sendMessage(ChatColor.GOLD+"-----------------------------------------------------");
		p.sendMessage(ChatColor.GREEN+""+ChatColor.ITALIC+"DirektServ est un serveur de jeux en constante évolution, merci de garder à l'esprit que notre staff travail bénévolement et que nous nous investissons corps et âmes dans ce serveur.\nIl se peut que vous découvriez un bug ou que votre expérience de jeu ne soit pas optimale, merci de nous en faire pars sur notre forum : http://forum.direktserv.fr/ \nVous pouvez soutenir le projet en achetant un grade via la boutique ou en faisant un don : http://store.direktserv.fr/");
		p.sendMessage(ChatColor.GOLD+"-----------------------------------------------------");
		try {
			int lastconnect = 1;
			if(SQLGestion.registerOnBDD(p) == false){
				DirektBungee.pcnt.add(p);
				DirektBungee.getInstance().getProxy().getPluginManager().dispatchCommand(DirektBungee.getInstance().getProxy().getConsole(), "bp user "+p.getName()+" addgroup minivip");
				p.sendMessage(ChatColor.RED+"Ravis de vous rencontrer, comme c'est votre première connexion nous vous offrons le Mini-VIP pour cette session.");
			}
			SQLGestion.setlastconnect(p, (lastconnect * 1000) - System.currentTimeMillis());
			if(!SQLGestion.getPseudo(p).equals(p.getName())){
				SQLGestion.setPseudo(p);
			}
			//set Grade
			if(SQLGestion.getDateBuyGrade(p) != null){
				DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
				Date date1 = dateFormat.parse(SQLGestion.getDateBuyGrade(p));
				String grade = SQLGestion.getGrade(p);

				Calendar cal1 = Calendar.getInstance();
				Calendar cal2 = Calendar.getInstance();

				cal1.setTime(date1);
				if(cal1.before(cal2)){
					SQLGestion.setDateBuyGrade(p, null);
					DirektBungee.getInstance().getProxy().getPluginManager().dispatchCommand(DirektBungee.getInstance().getProxy().getConsole(), "bp user "+p.getName()+" removegroup "+grade);
				}
			}

			//set Halo
			if(p.hasPermission("DirektServ.admin")){
				SQLGestion.setHalo(p, 5.0);
			}else if(p.hasPermission("DirektServ.assist")){
				SQLGestion.setHalo(p, 5.0);
			}else if(p.hasPermission("DirektServ.respdev")){
				SQLGestion.setHalo(p, 4.0);
			}else if(p.hasPermission("DirektServ.respmodo")){
				SQLGestion.setHalo(p, 4.0);
			}else if(p.hasPermission("DirektServ.resparchi")){
				SQLGestion.setHalo(p, 4.0);
			}else if(p.hasPermission("DirektServ.developpeur")){
				SQLGestion.setHalo(p, 4.0);
			}else if(p.hasPermission("DirektServ.staff")){
				SQLGestion.setHalo(p, 4.0);
			}else if(p.hasPermission("DirektServ.moderateur")){
				SQLGestion.setHalo(p, 3.0);
			}else if(p.hasPermission("DirektServ.architecte")){
				SQLGestion.setHalo(p, 3.0);
			}else if(p.hasPermission("DirektServ.helper")){
				SQLGestion.setHalo(p, 3.0);
			}else if(p.hasPermission("DirektServ.videaste")){
				SQLGestion.setHalo(p, 4.0);
			}else if(p.hasPermission("DirektServ.friend")){
				SQLGestion.setHalo(p, 3.0);
			}else if(p.hasPermission("DirektServ.donateur")){
				SQLGestion.setHalo(p, 3.0);
			}else if(p.hasPermission("DirektServ.vip")){
				SQLGestion.setHalo(p, 2.0);
			}else if(p.hasPermission("DirektServ.minivip")){
				SQLGestion.setHalo(p, 1.5);
			}else{
				if(SQLGestion.getDateBuyHalo(p) != null){
					DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
					Date date1 = dateFormat.parse(SQLGestion.getDateBuyHalo(p));
					
					Calendar cal1 = Calendar.getInstance();
					Calendar cal2 = Calendar.getInstance();
					
					cal1.setTime(date1);
		        	if(cal1.before(cal2)){
		        		SQLGestion.setDateBuyHalo(p, null);
		        		SQLGestion.setHalo(p, 1.0);
		        	}
				}else{
					SQLGestion.setHalo(p, 1.0);
				}
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		if(ProxyServer.getInstance().getOnlineCount() == 1){
			MainTask.Annonce();
		}
		if(MessageListener.jda.getStatus().equals(Status.CONNECTED)){
			SendMessage.sendToDiscord(MessageListener.jda, "s'est connecté.", p.getDisplayName(), "Bungee");
		}
	}

}
