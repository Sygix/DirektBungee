/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektBungee.Discord;

import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class SendMessage {
	
	public static void sendToDiscord(JDA jda, String message, String playername, String serveur){
        MessageChannel channel = jda.getTextChannelById("239619628253315072");
        channel.sendMessage("**["+serveur+"]** "+playername+" : "+message).queue();
	}
	
	public static void sendToDiscord(JDA jda, String message){
        MessageChannel channel = jda.getTextChannelById("239619628253315072");
        channel.sendMessage(message).queue();
	}
	
	@SuppressWarnings("deprecation")
	public static void sendToMinecraft(ServerInfo serveur, String author, String message){
		message = ChatColor.translateAlternateColorCodes('&', message);
        for(ProxiedPlayer p : serveur.getPlayers()){
        	p.sendMessage("�4[Discord] �3"+author+" : �r"+message);
        }
        message = ChatColor.stripColor(message);
        sendToDiscord(MessageListener.jda, "[Discord] "+author+" : "+message);
	}

}
